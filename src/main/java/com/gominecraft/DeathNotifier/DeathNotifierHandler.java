package com.gominecraft.DeathNotifier;

import java.util.List;
import java.util.Random;

import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

final class DeathNotifierHandler {

    private static DeathNotifier plugin = DeathNotifier.INSTANCE;

    private DeathNotifierHandler() {
    }

    static String handleDeath(Player player, PlayerDeathEvent event) {

        String message = chooseMessage(event, player);

        if (message.isEmpty()) {
            return "";
        }

        if (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent killer = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();

            Entity damager = killer.getDamager();

            if (damager instanceof Player) {
                String opponent = damager.getName();
                String weapon = "Fists";

                ItemStack item = ((Player) damager).getInventory().getItemInMainHand();

                if (item.getType() != Material.AIR && item.getType() != Material.CAVE_AIR && item.getType() != Material.VOID_AIR) {
                    if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
                        weapon = item.getItemMeta().getDisplayName();
                    } else {
                        weapon = item.getType().name().toLowerCase().replace("_", " ");
                    }
                }

                message = message.replace("%opponent", opponent).replace("%weapon", weapon);

            } else if (damager instanceof Wolf) {
                if (((Wolf) damager).isTamed()) {
                    message = message.replace("%opponent", ((Wolf) damager).getOwner().getName());
                }

            } else if (damager instanceof Projectile) {
                ProjectileSource shooter = ((Projectile) damager).getShooter();

                if (shooter instanceof Player) {
                    message = message.replace("%opponent", ((Player) shooter).getName());
                }
            }
        }

        message = message.replace("%player", player.getName());
        return message;
    }

    private static String chooseMessage(PlayerDeathEvent event, Player player) {

        String path = "Unknown";

        Biome biome = player.getLocation().getBlock().getBiome();

        if (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent killer = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();

            Entity damager = killer.getDamager();

            path = damager.getName().replace(" ", "");

            // Projectile stuff.
            if (damager instanceof Arrow) {
                Arrow arrow = (Arrow) killer.getDamager();
                if (arrow.getShooter() instanceof Player) {
                    path = "PVP.Ranged.Arrow";
                } else if (arrow.getShooter() instanceof Skeleton) {
                    path = "Skeleton";
                } else {
                    path = "Projectile.Arrow";
                }
            }
        }

        if (event.getEntity().getLastDamageCause() instanceof EntityDamageEvent) {

            EntityDamageEvent killer = event.getEntity().getLastDamageCause();
            EntityDamageEvent.DamageCause damager = killer.getCause();

            switch (damager) {
                case CONTACT:
                    path = "Cactus";
                    break;
                case DROWNING:
                    path = "Drowning";

                    if (biome == Biome.OCEAN || biome == Biome.DEEP_OCEAN || biome == Biome.DEEP_COLD_OCEAN || biome == Biome.DEEP_FROZEN_OCEAN || biome == Biome.DEEP_LUKEWARM_OCEAN || biome == biome.DEEP_WARM_OCEAN) {
                        path = path + ".Ocean";
                    } else {
                        path = path + ".Water";
                    }
                    break;
                case FALL:
                    path = "Fall";
                    break;
                case FIRE:
                case FIRE_TICK:
                    path = "Fire";
                    break;
                case FLY_INTO_WALL:
                    path = "Fly_Into_Wall";
                    break;
                case HOT_FLOOR:
                    path = "MagmaBlock";
                    break;
                case LAVA:
                    path = "Lava";
                    break;
                case POISON:
                    path = "Poison";
                    break;
                case SUICIDE:
                    path = "Suicide";
                    break;
                case SUFFOCATION:
                    path = "Suffocation";
                    break;
                case STARVATION:
                    path = "Starvation";
                    break;
                case VOID:
                    path = "Void";
                    break;
                case WITHER:
                    path = "Wither";
                    break;
                case ENTITY_ATTACK:
                    EntityDamageByEntityEvent event1 = (EntityDamageByEntityEvent) killer;
                    if (event1.getDamager() instanceof Wolf) {
                        Wolf wolf = (Wolf) event1.getDamager();
                        if (wolf.isTamed()) {
                            path = "PVP.Pet.Wolf";
                        } else {
                            path = "Wolf";
                        }
                    } else if (event1.getDamager() instanceof Projectile) {
                        Projectile projectile = (Projectile) event1.getDamager();
                        if (projectile.getShooter() instanceof Player) {
                            path = "PVP.Ranged.Bow";
                        } else if (projectile.getShooter() instanceof Skeleton) {
                            path = "Skeleton";
                        }
                    } else if (event1.getDamager() instanceof Player) {
                        path = "PVP.Melee";
                    }
                    break;
                default:
                    plugin.getLogger().info("Unknown/Uncaught Cause: " + damager.toString());
                    plugin.getLogger().info("Report the above line to the developer.");
            }
        }

        if (plugin.getMessages().get(path, null) == null) {
            return "No message found in message file for " + path;
        }

        List<String> messages = plugin.getMessages().getStringList(path);
        return (!messages.isEmpty()) ? messages.get(new Random().nextInt(messages.size())) : "";
    }
}
