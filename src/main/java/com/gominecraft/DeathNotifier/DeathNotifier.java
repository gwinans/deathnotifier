package com.gominecraft.DeathNotifier;

import java.io.File;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class DeathNotifier extends JavaPlugin {

    static DeathNotifier INSTANCE;

    private YamlConfiguration messages;

    YamlConfiguration getMessages() {
        return messages;
    }

    void loadMessages() {
        String lang = getConfig().getString("lang");

        File messagesLangFile = new File(getDataFolder(), "lang/" + lang + ".yml");

        if (!messagesLangFile.exists()) {
            getLogger().info("Unable to find lang/" + lang + ".yml. Defaulting to lang/en_US.yml");
            lang = "en_US";
            messagesLangFile = new File(getDataFolder(), "lang/en_US.yml");
        } else {
            getLogger().info("Found " + lang + ".yml, loading ...");
        }

        messages = YamlConfiguration.loadConfiguration(messagesLangFile);

    }

    private void setupPlugin() {
        Metrics metrics = new Metrics(this);

        getServer().getPluginManager().registerEvents(new DeathNotifierListener(), this);
        getCommand("dnreload").setExecutor(new DeathNotifierReloadCommand());
    }

    @Override
    public void onDisable() {
    } // Leaving this for now.

    @Override
    public void onEnable() {
        if (INSTANCE == null) {
            INSTANCE = this;
        }

        // Save default configs.
        saveDefaultConfig();

        // We always overwrite en_US.yml, just in case!
        saveResource("lang/en_US.yml", true);
        // We always overwrite the Template.
        saveResource("lang/template.yml", true);
        loadMessages();
        setupPlugin();
    }

}