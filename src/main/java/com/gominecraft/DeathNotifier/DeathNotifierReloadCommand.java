package com.gominecraft.DeathNotifier;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

class DeathNotifierReloadCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender.hasPermission("deathnotifier.reload")) {
            DeathNotifier plugin = DeathNotifier.INSTANCE;

            plugin.reloadConfig();
            String lang = plugin.getConfig().getString("lang");

            if (lang.equalsIgnoreCase("en_US")) {
                sender.sendMessage(ChatColor.RED + "You appear to be using the default en_US.yml. This file is overwritten every time the server starts up.");
                sender.sendMessage(ChatColor.RED + "Make a different file in DeathNotifier/lang/ and modify config.yml.");
            }

            plugin.loadMessages();

            String success = "Messages have been reloaded from lang/" + lang + ".yml!";

            sender.sendMessage(ChatColor.GREEN + success);
            plugin.getLogger().info(success);
        } else {
            sender.sendMessage(ChatColor.RED + "You do not have permission to execute this command.");
        }
        return true;
    }
}